<?php
	/**
	 * The Template for displaying all single posts.
	 *
	 * @package Bezel
	 */
	
	get_header(); ?>

<div class="container">
	<div class="row">
		
		<section id="breadcrumbs" class="col-xs-12 col-lg-12" style="margin-bottom:20px;">
			<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
			?>
		</section>
		
		<div id="primary" class="content-area <?php bezel_layout_class( 'content' ); ?>">
			<main id="main" class="site-main">
				
				<?php widget_template__before_content(); ?>
				
				<div id="post-wrapper" class="post-wrapper post-wrapper-single">
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'template-parts/content', 'single' ); ?>
						
						<?php widget_template__after_content(); ?>
						
						<?php bezel_the_post_navigation(); ?>
						
						<?php widget_template__bofore_comments(); ?>
						<?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || '0' != get_comments_number() ) :
							comments_template();
						endif;
						?>
					<?php endwhile; // end of the loop. ?>
				</div><!-- .post-wrapper -->
			</main><!-- #main -->
		</div><!-- #primary -->
		
		<?php get_sidebar(); ?>
	
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
<script>
	function ya_share() {
		var len = jQuery('.video__description').length;
		
		if (len == 1) {
			jQuery('.video__description').before('<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,pinterest,twitter,telegram" data-image="<?php the_post_thumbnail_url( "large" ); ?> "></div>');
		}
		
		if (len > 1) {
			jQuery('.video__description:first-child').before('<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,pinterest,twitter,telegram" data-image="<?php the_post_thumbnail_url( "large" ); ?> "></div>');
		}
	}
	
	ya_share();
</script>

