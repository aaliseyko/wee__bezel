<?php
	define( 'ROOT', $_SERVER['DOCUMENT_ROOT'] );
	define( 'ROOT_URI', 'https://' . $_SERVER['SERVER_NAME'] );
	define( 'ROOT_ASSETS', ROOT . '/assets' );
	define( 'THEME_PATH', get_stylesheet_directory() );
	define( 'THEME_URI', get_stylesheet_directory_uri() );
	define( 'THEME_IMAGES', THEME_URI . '/images' );
	define( 'THEME_CSS', THEME_URI . '/css' );
	define( 'THEME_JS', THEME_URI . '/js' );
