<?php
	function debug( $var, $dump = false, $die = false )
	{
		echo '<pre>';
		if ( ! $dump ) {
			print_r( $var );
		} else {
			var_dump( $var );
		}
		if ( $die ) {
			die( 'App Killed by DEBUG' );
		}
		echo '</pre>';
	}
