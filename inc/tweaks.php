<?php
	//----------------  WP Tweaks below	----------------
	/**
	 *
	 * @param $link
	 *
	 * @return mixed
	 */
	function remove_more_jump_link( $link )
	{
		$offset = strpos( $link, '#more-' );
		if ( $offset ) {
			$end = strpos( $link, '"', $offset );
		}
		if ( $end ) {
			$link = substr_replace( $link, '', $offset, $end - $offset );
		}
		
		return $link;
	}
	
	add_filter( 'the_content_more_link', 'remove_more_jump_link' );
	
	/**
	 * Disable WP emoji
	 */
	
	function disable_emojicons_tinymce( $plugins )
	{
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		} else {
			return array();
		}
	}
	
	function disable_wp_emojicons()
	{
		// all actions related to emojis
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		
		// filter to remove TinyMCE emojis
		add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
	}
	
	add_action( 'init', 'disable_wp_emojicons' );
	add_filter( 'emoji_svg_url', '__return_false' );
	
	/**
	 * disable xml-rpc
	 */
	function remove_x_pingback( $headers )
	{
		unset( $headers['X-Pingback'] );
		
		return $headers;
	}
	
	add_filter( 'wp_headers', 'remove_x_pingback' );
	add_filter( 'xmlrpc_enabled', '__return_false' );
	
	/**
	 * Do not ping
	 *
	 * @param $links
	 */
	function no_self_ping( &$links )
	{
		$home = get_option( 'home' );
		foreach ( $links as $l => $link ) {
			if ( 0 === strpos( $link, $home ) ) {
				unset( $links[ $l ] );
			}
		}
	}
	
	add_action( 'pre_ping', 'no_self_ping' );
	
	
	/**
	 * <Head> section cleanup
	 */
	remove_action( 'wp_head', 'wlwmanifest_link');
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wp_generator' );
	
	add_filter( 'feed_links_show_comments_feed', '__return_false' );
