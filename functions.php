<?php
	require_once 'inc/debug.php';
	require_once 'inc/define.php';
	require 'inc/tweaks.php';
	
	if ( ! function_exists( 'bezel_setup' ) ) :
		/**
		 * Sets up theme defaults and registers support for various WordPress features.
		 *
		 * Note that this function is hooked into the after_setup_theme hook, which
		 * runs before the init hook. The init hook is too late for some features, such
		 * as indicating support for post thumbnails.
		 */
		function bezel_setup() {
			/*
			 * Make theme available for translation.
			 * Translations can be filed in the /languages/ directory.
			 * If you're building a theme based on Bezel, use a find and replace
			 * to change 'bezel' to the name of your theme in all the template files
			 */
			load_theme_textdomain( 'bezel', get_template_directory() . '/languages' );
			
			// Add default posts and comments RSS feed links to head.
			add_theme_support( 'automatic-feed-links' );
			
			/*
			 * Let WordPress manage the document title.
			 * By adding theme support, we declare that this theme does not use a
			 * hard-coded <title> tag in the document head, and expect WordPress to
			 * provide it for us.
			 */
			add_theme_support( 'title-tag' );
			
			/*
			 * Enable support for custom logo.
			 *
			 * @see https://developer.wordpress.org/reference/functions/add_theme_support/#custom-logo
			 */
			add_theme_support( 'custom-logo', array(
				'height'      => 400,
				'width'       => 560,
				'flex-height' => true,
				'flex-width'  => true,
				'header-text' => array( 'site-title', 'site-description' ),
			) );
			
			/*
			 * Enable support for Post Thumbnails on posts and pages.
			 *
			 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
			 */
			add_theme_support( 'post-thumbnails' );
			
			// Theme Image Sizes
			add_image_size( 'bezel-featured', 1000, 565, true );
			
			// This theme uses wp_nav_menu() in four locations.
			register_nav_menus( array(
				'primary' => esc_html__( 'Primary Menu', 'bezel' ),
			) );
			
			/*
			 * Switch default core markup for search form, comment form, and comments
			 * to output valid HTML5.
			 */
			add_theme_support( 'html5', array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption'
			) );
			
			// Setup the WordPress core custom background feature.
			add_theme_support( 'custom-background', apply_filters( 'bezel_custom_background_args', array(
				'default-color' => 'f2f2f0',
				'default-image' => '',
			) ) );
			
		}
	endif; // bezel_setup
	add_action( 'after_setup_theme', 'bezel_setup' );
	
	/**
	 * Set the content width in pixels, based on the theme's design and stylesheet.
	 *
	 * Priority 0 to make it available to lower priority callbacks.
	 *
	 * @global int $content_width
	 */
	function bezel_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'bezel_content_width', 317 );
	}
	
	add_action( 'after_setup_theme', 'bezel_content_width', 0 );
	
	/**
	 * Register widget area.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
	 */
	function bezel_widgets_init() {
		
		// Widget Areas
		register_sidebar( array(
			'name'          => esc_html__( 'Main Sidebar', 'bezel' ),
			'id'            => 'sidebar-1',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
		
		// before content widget
		register_sidebar( array(
			'name'          => esc_html__( 'Before Content', 'bezel' ),
			'id'            => 'before_content',
			'before_widget' => '<aside id="%1$s" class="%2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
		
		
		// before content widget
		register_sidebar( array(
			'name'          => esc_html__( 'After Content', 'bezel' ),
			'id'            => 'after_content',
			'before_widget' => '<aside id="%1$s" class="%2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
		
		// before comments widget
		register_sidebar( array(
			'name'          => esc_html__( 'Before Comments', 'bezel' ),
			'id'            => 'before_comments',
			'before_widget' => '<aside id="%1$s" class="%2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
		
	}
	
	add_action( 'widgets_init', 'bezel_widgets_init' );
	
	
	/**
	 * Built-in jQuery enabling (into footer if possible)
	 */
	function starter_scripts() {
		wp_deregister_script( 'jquery' );
	}
	
	add_action( 'wp_enqueue_scripts', 'starter_scripts' );
	
	/**
	 * Bootsrap v.3.7
	 */
	function wee__bezel_bootstrap() {
		wp_register_script( 'bootstrap', '/assets/js/bootstrap.min.js', array( 'jquery' ), '3.7', true );
		wp_enqueue_script( 'bootstrap' );
	}
	
	//add_action( 'wp_enqueue_scripts', 'wee__bezel_bootstrap' );
	
	
	/**
	 * Enqueue scripts and styles.
	 */
	function bezel_scripts() {
		/**
		 * Enqueue JS files
		 */
		// Assets Libs (jQ, migrate, bootstrap)
		wp_enqueue_script( 'assets-libs', THEME_URI . '/assets/js/libs.js', array(), '17623.1201', false );
		
		wp_enqueue_script( 'libs', THEME_URI. '/js/libs.js', array( 'assets-libs' ), '17623.1151.', true );
		
		// Comment Reply
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		
		// Custom Script
		wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array( 'libs' ), '1.0', true );
		
		/**
		 * Enqueue CSS files
		 */
		$bootstrap_grid_version = date( 'ynj.gi', filemtime( THEME_PATH . '/assets/css/grid.css' ) );
		wp_enqueue_style( 'bootstrap-grid', THEME_URI . '/assets/css/grid.css', array(), $bootstrap_grid_version );
	}
	
	add_action( 'wp_enqueue_scripts', 'bezel_scripts' );
	
	function wee__bezel_footer_styles() {
		$bootstrap_version = date( 'ynj.gi', filemtime( THEME_PATH . '/assets/css/bootstrap.css' ) );
		wp_enqueue_style( 'bootstrap', THEME_URI . '/assets/css/bootstrap.css', [], $bootstrap_version );
		
		// Theme Stylesheet
		$styles_version = date( 'ynj.gi', filemtime( THEME_PATH . '/css/styles.css' ) );
		wp_enqueue_style( 'styles', THEME_URI . '/css/styles.css', array( 'bootstrap' ), $styles_version );
		
		// FAwesome
		$assets_version = date( 'ynj.gi', filemtime( THEME_PATH . '/assets/css/assets.css' ) );
		wp_enqueue_style( 'libs', THEME_URI . '/assets/css/assets.css', [ 'styles' ], $assets_version );
	}
	
	;
	add_action( 'get_footer', 'wee__bezel_footer_styles' );
	
	/**
	 * Custom functions that act independently of the theme templates.
	 */
	require 'inc/extras.php';
	
	/**
	 * Custom template tags for this theme.
	 */
	require 'inc/template-tags.php';
	
	/**
	 * Implement the Custom Header feature.
	 */
	require 'inc/custom-header.php';
	
	/**
	 * Customizer additions.
	 */
	require 'inc/customizer.php';
	
	require 'inc/plugins.php';
	
