<?php get_header(); ?>
<!-- home -->
<div class="container">
	<div class="row">
		<div id="primary" class="content-area <?php bezel_layout_class( 'content' ); ?>">
			<main id="main" class="site-main">
				<?php if ( have_posts() ) : ?>
					<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; ?>
					<?php query_posts( 'posts_per_page=12&paged=' . $paged ); ?>
					<div id="post-wrapper" class="post-wrapper post-wrapper-archive">
						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'template-parts/content', 'home' ); ?>
						<?php endwhile; ?>
					</div><!-- .post-wrapper -->
					
					<?php widget_template__after_content(); ?>
					
					<?php bezel_the_posts_pagination(); ?>
				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>
			</main><!-- #main -->
		</div><!-- #primary -->
		<?php get_sidebar(); ?>
	</div><!-- .row -->
</div><!-- .container -->
<?php get_footer(); ?>
