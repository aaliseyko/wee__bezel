<?php
/**
 * Template part for displaying single posts.
 *
 * @package Bezel
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post-content-wrapper post-content-wrapper-single">
		<div class="entry-data-wrapper entry-data-wrapper-single">

			<div class="entry-header-wrapper">
				<div class="entry-meta entry-meta-header-before">
					<ul>
						<li><?php bezel_posted_on(); ?></li>
					</ul>
				</div><!-- .entry-meta -->

				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->
			</div><!-- .entry-header-wrapper -->
			
			<div class="entry-content">
				<?php the_content(); ?>
				
				<!--<div rel="author" class="hidden"><?/*=get_the_author();*/?></div>-->
				
				<?php //if ( get_the_author_meta( 'description' ) ) : ?>
					<div rel="author" class="author-box hidden">
						<div class="author-img"><?php echo get_avatar( get_the_author_meta( 'user_email' ), '100' ); ?></div>
						<h4 class="author-name"><?php the_author_meta( 'display_name' ); ?></h4>
						<p class="author-description"><?php the_author_meta( 'description' ); ?></p>
					</div>
				<?php //endif; ?>
				
				
				<?php
					wp_link_pages( array(
						'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'bezel' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
					) );
				?>
			</div><!-- .entry-content -->

			<footer class="entry-meta entry-meta-footer">
				<?php bezel_entry_footer(); ?>
			</footer><!-- .entry-meta -->

		</div><!-- .entry-data-wrapper -->
	</div><!-- .post-content-wrapper -->
</article><!-- #post-## -->
