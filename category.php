<?php get_header(); ?>
<!-- category -->
<div class="container">
	<div class="row">
		<section id="breadcrumbs" class="col-xs-12 col-lg-12" style="margin-bottom:20px;">
			<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
			?>
		</section>
		<section id="primary" class="content-area <?php bezel_layout_class( 'content' ); ?>">
			<main id="main" class="site-main">
				<?php if ( have_posts() ) : ?>
					<?php
					$cat_id = get_query_var( 'cat' );
					$cat    = get_category( $cat_id, OBJECT );
					?>
					<?php if ( $cat->name OR $cat->description ) : ?>
						<header class="page-header">
							<?php if ( $cat->name ): ?>
								<h1 class="page-title"><?= $cat->name; ?></h1>
							<?php endif; ?>
							
							<?php if ( $cat->description ): ?>
								<div class="taxonomy-description"><?= $cat->description; ?></div>
							<?php endif; ?>
						</header>
					<?php endif; ?>
					
					<?php widget_template__before_content(); ?>
					
					<div id="post-wrapper" class="post-wrapper post-wrapper-archive">
						<?php /* Start the Loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php
							/* Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_format() );
							?>
						<?php endwhile; ?>
					</div><!-- .post-wrapper -->
					
					<?php bezel_the_posts_pagination(); ?>
					
					<?php widget_template__after_content(); ?>
				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>
			
			</main><!-- #main -->
		</section><!-- #primary -->
		
		<?php get_sidebar(); ?>
	
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
