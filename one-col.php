<?php
	/**
	 * Template Name: Featured Article
	 * Template Post Type: post, page, product
	 */
	
	get_header(); ?>

<div class="container">
	<div class="row">
		
		<section id="breadcrumbs" class="col-xs-12 col-lg-12" style="margin-bottom:20px;">
			<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
			?>
		</section>
		
		<div id="primary" class="content-area col-xs-12">
			<main id="main" class="site-main">
				<div id="post-wrapper" class="post-wrapper post-wrapper-single">
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'template-parts/content', 'shop' ); ?>
					<?php endwhile; // end of the loop. ?>
				</div><!-- .post-wrapper -->
			</main><!-- #main -->
		</div><!-- #primary -->
	
	</div><!-- .row -->
</div><!-- .container -->

<?php get_footer(); ?>
<script>
	function ya_share() {
		var len = jQuery('.video__description').length;
		
		if (len == 1) {
			jQuery('.video__description').before('<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,pinterest,twitter,telegram" data-image="<?php the_post_thumbnail_url( "large" ); ?> "></div>');
		}
		
		if (len > 1) {
			jQuery('.video__description:first-child').before('<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus,pinterest,twitter,telegram" data-image="<?php the_post_thumbnail_url( "large" ); ?> "></div>');
		}
	}
	
	ya_share();

</script>

